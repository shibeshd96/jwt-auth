var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const exjwt = require('express-jwt');

const PORT = process.env.PORT || 3001;
var app = express();
var db = require("./models");

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

const jwtMW = exjwt({
  secret: 'four tuples'
});

app.post('/signup', (req, res) => {
  const { username, password } = req.body;
  const saltRounds = 10;
  bcrypt.hash(password, saltRounds, function (err, hash) {
    db.user.create({
      username: username,
      password: hash
    }).then((result) => {
      console.log("User created: ", result);
      res.json("user created");
    })
  });
})

app.post('/log-in', (req, res) => {
  const { username, password } = req.body;
  console.log("User submitted: ", username, password);

  db.user.findOne(
    {
      where: { username: username }
    })
    .then((user) => {
      console.log("User Found: ", user);
      if (user === null) {
        res.status(401).json({
          sucess: false,
          token: null,
          err: 'Invalid Credentials'
        });
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          console.log("Valid!");

          let token = jwt.sign(
            {
              username: user.username
            },
            'four tuples',
            { expiresIn: 129600 }); 
          
          res.json({
            sucess: true,
            err: null,
            token
          });
        }
        else {
          console.log("Incorrect");
          res.status(401).json({
            sucess: false,
            token: null,
            err: 'Password Milena'
          });
        }
      });
    })
});

app.get('/', jwtMW, (req, res) => {
  console.log("Web Token Checked.")
  res.send('La Thik Chha'); 
});

db.sequelize.sync().then(() => {
  app.listen(PORT, function () {
    console.log("App listening on PORT " + PORT);
  });
})


module.exports = app;